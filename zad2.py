'''

2. Incijalizirate tri GPUArray-a oblika 10 * 10, i nad prva dva izvedite operaciju

1.5 * exp(a) + 4.2 * log(b) + 6.1

rezultat spremite u treci.


'''

import pycuda.autoinit
import pycuda.driver as drv
import pycuda.gpuarray as ga
import pycuda.cumath as cm
import numpy as np
from pycuda.compiler import SourceModule

a = np.ones((10, 10), dtype=np.float32)
b = np.ones((10, 10), dtype=np.float32)
c = np.zeros((10, 10), dtype=np.float32)

print(a)
print(b)

# dodajte kod
a_gpu = ga.to_gpu(a)
b_gpu = ga.to_gpu(b)
c_gpu = ga.to_gpu(c)

c_gpu = 1.5 * cm.exp(a_gpu) + 4.2 * cm.log(b_gpu) + 6.1

print(c_gpu.get())
