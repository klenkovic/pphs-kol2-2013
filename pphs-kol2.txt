1. Napišite program koji vrši jednoblokovno modificirano zbrajanje matrica formata 10 * 10 tako da se svaki element računa po formuli

1.5 * exp(a) + 4.2 * log(b) + 6.1

Ovu operaciju modificiranog zbrajanja učinite device funkcijom i pozovite ju unutar zrna.

2. Incijalizirate tri GPUArray-a oblika 10 * 10, i nad prva dva izvedite operaciju

1.5 * exp(a) + 4.2 * log(b) + 6.1

rezultat spremite u treći.

3. Inicijalizirajte tri GPUArray-a; prvi neka bude sa 10000 nekvalitetnih slučajnih brojeva, drugi sa 10000 kvalitetnih slučajnih brojeva generiranih XORWOW generatorom, treći sa 10000 kvalitetnih slučajnih brojeva generiranih MRG32k3a generatorom.

Svaki od njih dohvatite u numpy polje i na CPU-u odredite i ispišite na ekran srednju vrijednost (mean), koja je suma vrijednosti podijeljena sa brojem vrijednosti.
