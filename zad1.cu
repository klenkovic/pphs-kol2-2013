#include <math.h>

// prilagodite parametre
__device__ float custom_sum (float a, float b)
{
  // dopunite tijelo funkcije
  return 1.5 * exp(a) + 4.2 * log(b) + 6.1;
}

// prilagodite parametre
__global__ void matrix_operation (float *a, float *b, float *c)
{
  // postavite na adekvatnu vrijednost
  const int idx = threadIdx.y * blockDim.x + threadIdx.x;

  // dopunite tijelo funkcije

  c[idx] = custom_sum(a[idx], b[idx]);

}
