'''

Inicijalizirajte tri GPUArray-a; prvi neka bude sa 10000 nekvalitetnih slucajnih brojeva, drugi sa 10000 kvalitetnih slucajnih brojeva generiranih XORWOW generatorom, treci sa 10000 kvalitetnih slucajnih brojeva generiranih MRG32k3a generatorom.

Svaki od njih dohvatite u numpy polje i na CPU-u odredite i ispisite na ekran srednju vrijednost (mean), koja je suma vrijednosti podijeljena sa brojem vrijednosti.

'''

import pycuda.autoinit
import pycuda.driver as drv
import pycuda.gpuarray as ga
import pycuda.curandom as cr
import numpy as np
from pycuda.compiler import SourceModule

# dodajte kod
#cr.XORWOWRandomNumberGenerator()

a_gpu =  cr.rand(10000)
#print(a_gpu.get())
b_gen = cr.XORWOWRandomNumberGenerator()
b_gpu = b_gen.gen_uniform(10000, dtype=np.float32)
#print(b_gpu.get())
c_gen = cr.MRG32k3aRandomNumberGenerator()
c_gpu = c_gen.gen_uniform(10000, dtype=np.float32)
#print(c_gpu.get())

# dodajte kod
a = a_gpu.get()
b = b_gpu.get()
c = c_gpu.get()

a_mean = a.mean()
b_mean = b.mean()
c_mean = c.mean()

print(a_mean)
print(b_mean)
print(c_mean)

a_izr = a.sum() / a_mean
b_izr = b.sum() / b_mean
c_izr = c.sum() / c_mean

print(a_izr)
print(b_izr)
print(c_izr)
