'''
1. Napisite program koji vrsi jednoblokovno modificirano zbrajanje matrica formata 10 * 10 tako da se svaki element racuna po formuli

1.5 * exp(a) + 4.2 * log(b) + 6.1

Ovu operaciju modificiranog zbrajanja ucinite device funkcijom i pozovite ju unutar zrna.

'''

import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule(open("zad1.cu").read())

matrix_operation = mod.get_function("matrix_operation")

a = np.ones((10, 10), dtype=np.float32)
b = np.ones((10, 10), dtype=np.float32)
c = np.zeros((10, 10), dtype=np.float32)

# prilagodite poziv funkcije svojim potrebama
matrix_operation(drv.In(a), drv.In(b), drv.Out(c), block=(10,10,1), grid=(1,1))

print(c)
